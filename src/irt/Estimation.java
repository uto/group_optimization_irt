package irt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.uncommons.maths.random.GaussianGenerator;

import utility.MyUtil;

public class Estimation {
	private ArrayList<double[]> AlphaT_Ests, AlphaR_Ests, TauR_Ests, Theta_Ests;
	private ArrayList<double[][]> betaTK_Ests;

	private int maxLoop, burnIn, interbal;
	private IRTModel irt;
	private Random rand;
	private GaussianGenerator proposalDist;
	private double proposalSD = 0.05;

	public Estimation(IRTModel irt, Random rand, HashMap<String, Integer> settings) {
		this.irt = irt; this.rand = rand;
		this.maxLoop = settings.get("maxloop");
		this.burnIn = settings.get("burnin");
		this.interbal = settings.get("interbal");
		this.proposalDist =  new GaussianGenerator(0.0, proposalSD, rand);

		AlphaT_Ests = new ArrayList<double[]>();
		AlphaR_Ests = new ArrayList<double[]>();
		betaTK_Ests = new ArrayList<double[][]>();
		TauR_Ests = new ArrayList<double[]>();
		Theta_Ests = new ArrayList<double[]>();

		System.out.println("Estimating all IRT parameters");
		for(int i=0; i<this.maxLoop; i++){
			if(i % (maxLoop/10) == 0) System.out.println(" >> MCMC loop: " + i + " / " + maxLoop);
			updateThetaParam();
			updateAlphaT();
			updateBetaTK();
			updateAlphaR();
			updateTauR();
			if(i >= this.burnIn && (i%this.interbal) == 0){
				this.addMCMCSamples();
			}
		}
		calcPointEstimation();
		System.out.println("Estimation Finished");
		System.out.println("--------------------------");
	}

	void addMCMCSamples(){
		AlphaT_Ests.add(irt.alphaT.clone());
		AlphaR_Ests.add(irt.alphaR.clone());
		double[][] betaTKtmp = new double[irt.betaTK.length][];
		for(int i=0;i<irt.betaTK.length;i++){
			betaTKtmp[i] = irt.betaTK[i].clone();
		}
		betaTK_Ests.add(betaTKtmp);
		TauR_Ests.add(irt.tauR.clone());
		Theta_Ests.add(irt.theta.clone());
	}

	void calcPointEstimation() {
		irt.alphaT = MyUtil.getAverages(AlphaT_Ests);
		irt.alphaR = MyUtil.getAverages(AlphaR_Ests);
		irt.betaTK = MyUtil.getAveMatrix(betaTK_Ests);
		irt.tauR = MyUtil.getAverages(TauR_Ests);
		irt.theta = MyUtil.getAverages(Theta_Ests);
	}

	private double posteriorTheta(int j) {
		return getLogLikelihoodTheta(j) + Math.log(MyUtil.getGausianValue(irt.theta_prior[0], irt.theta_prior[1], irt.theta[j]));
	}

	private double posteriorAlphaT(int t) {
		return getLogLikelihoodTask(t) + Math.log(MyUtil.getGausianValue(irt.alphaT_prior[0], irt.alphaT_prior[1], irt.alphaT[t]));
	}

	private double posteriorBetaTK(int t) {
		return getLogLikelihoodTask(t)
				+ Math.log(MyUtil.getMultGausianValue(irt.betaTK_priorMean, irt.betaTK_priorSD, MyUtil.removeDimension4GRM(irt.betaTK[t])));
	}

	private double posteriorAlphaR(int r) {
		return getLogLikelihoodRater(r) + Math.log(MyUtil.getGausianValue(irt.alphaR_prior[0], irt.alphaR_prior[1], irt.alphaR[r]));
	}

	private double posteriorTauR(int r) {
		return getLogLikelihoodRater(r) + Math.log(MyUtil.getGausianValue(irt.tauR_prior[0], irt.tauR_prior[1], irt.tauR[r]));
	}

	private double getLogLikelihoodTheta(int j) {
		double LogLikelihood = 0.0;
		for (int t = 0; t < irt.T; t++) {
			for (int r = 0; r < irt.R; r++) {
				LogLikelihood += Math.log(irt.getProb(j, t, r));
			}
		}
		return LogLikelihood;
	}

	private double getLogLikelihoodTask(int i) {
		double LogLikelihood = 0.0;
		for (int j = 0; j < irt.J; j++) {
			for (int r = 0; r < irt.R; r++) {
				LogLikelihood += Math.log(irt.getProb(j, i, r));
			}
		}
		return LogLikelihood;
	}

	private double getLogLikelihoodRater(int r) {
		double LogLikelihood = 0.0;
		for (int j = 0; j < irt.J; j++) {
			for (int t = 0; t < irt.T; t++) {
				LogLikelihood += Math.log(irt.getProb(j, t, r));
			}
		}
		return LogLikelihood;
	}

	public void updateAlphaT() {
		for (int t = 0; t < irt.T; t++) {
			double prevAlpha = irt.alphaT[t];
			double prevLikelihood = posteriorAlphaT(t);
			irt.alphaT[t] += proposalDist.nextValue();
			double newLikelihood = posteriorAlphaT(t);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.alphaT[t] = prevAlpha;
			}
		}
	}

	public void updateBetaTK() {
		for (int t = 0; t < irt.T; t++) {
			double prevBeta[] = irt.betaTK[t].clone();
			double prevLikelihood = posteriorBetaTK(t);
			for (int k = 0; k < irt.K - 1; k++) {
				irt.betaTK[t][k] += proposalDist.nextValue();
				if (k == 0) {
					if (irt.betaTK[t][k] > irt.betaTK[t][k + 1]) {
						irt.betaTK[t][k] = prevBeta[k];
					}
				} else if (k == (irt.K - 2)) {
					if (irt.betaTK[t][k - 1] > irt.betaTK[t][k]) {
						irt.betaTK[t][k] = prevBeta[k];
					}
				} else {
					if (irt.betaTK[t][k - 1] > irt.betaTK[t][k] || irt.betaTK[t][k] > irt.betaTK[t][k + 1]) {
						irt.betaTK[t][k] = prevBeta[k];
					}
				}
			}
			double newLikelihood = posteriorBetaTK(t);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.betaTK[t] = prevBeta.clone();
			}
		}
	}

	public void updateAlphaR(){
		double[] prevAlphaR = irt.alphaR.clone();
		double prod = 0.0;
		double prevLikelihood = 0.0;
		for(int r=0; r<irt.R; r++){
			prevLikelihood += posteriorAlphaR(r);
			irt.alphaR[r] += proposalDist.nextValue();
			prod += irt.alphaR[r];
		}
		prod = prod / irt.R;
		double newLikelihood = 0.0;
		for(int r=0; r<irt.R; r++){
			irt.alphaR[r] = irt.alphaR[r] - prod;
			newLikelihood += posteriorAlphaR(r);
		}
		double thresh = Math.exp(newLikelihood - prevLikelihood);
		if( MyUtil.isRejected(thresh, rand) == true){
			irt.alphaR = prevAlphaR.clone();
		}
		prevAlphaR = null;
	}

	public void updateTauR(){
		double[] prevTauR = irt.tauR.clone();
		double prod = 0.0;
		double prevLikelihood = 0.0;
		for(int r=0; r<irt.R; r++){
			prevLikelihood += posteriorTauR(r);
			irt.tauR[r] += proposalDist.nextValue();
			prod += irt.tauR[r];
		}
		prod = prod / irt.R;
		double newLikelihood = 0.0;
		for(int r=0; r<irt.R; r++){
			irt.tauR[r] = irt.tauR[r] - prod;
			newLikelihood += posteriorTauR(r);
		}
		double thresh = Math.exp(newLikelihood - prevLikelihood);
		if( MyUtil.isRejected(thresh, rand) == true){
			irt.tauR = prevTauR.clone();
		}
		prevTauR = null;
	}

	public void updateThetaParam() {
		for (int j = 0; j < irt.J; j++) {
			double prevTheta = irt.theta[j];
			double prevLikelihood = posteriorTheta(j);
			irt.theta[j] += proposalDist.nextValue();
			double newLikelihood = posteriorTheta(j);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.theta[j] = prevTheta;
			}
		}
	}

}
