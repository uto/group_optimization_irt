package irt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

import utility.MyUtil;

public class Data {
	public int[][][] U;// j, t, r
	private IRTModel irt;

	public Data(IRTModel irt, Random rand) {
		this.irt = irt;
		U = new int[irt.J][irt.T][irt.R];
		for (int j = 0; j < irt.J; j++) {
			for (int t = 0; t < irt.T; t++) {
				for (int r = 0; r < irt.R; r++) {
					U[j][t][r] = -1;
				}
			}
		}
	}

	public void readData(String path) {
		try {
			BufferedReader br = MyUtil.Reader(path);
			for (int t = 0; t < irt.T; t++) {
				for (int j = 0; j < irt.J; j++) {
					for (int r = 0; r < irt.R; r++) {
						String[] line  = br.readLine().split(",");
						U[j][t][r] = Integer.valueOf(line[3]);
					}
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setMissingT(int t){
		for (int j = 0; j < irt.J; j++) {
			for (int r = 0; r < irt.R; r++) {
				U[j][t][r] = -1;
			}
		}
	}

	public void setGrouping(int[][][] groups, int G, int t) {
		for (int j = 0; j < irt.J; j++) {
			for (int g = 0; g < G; g++) {
				if (groups[t][g][j] == 1) {
					for (int r = 0; r < irt.R; r++) {
						if (r == j || groups[t][g][r] == 0) {
							U[j][t][r] = -1;
						}
					}
					break;
				}
			}
		}
	}

	public void setGroupingWithExternal(int[][][] groups, int[][][] selectedRaters, int G, int t) {
		for (int j = 0; j < irt.J; j++) {
			for (int g = 0; g < G; g++) {
				if (groups[t][g][j] == 1) {
					for (int r = 0; r < irt.R; r++) {
						if (r == j || (groups[t][g][r] == 0 && selectedRaters[t][j][r] == 0)) {
							U[j][t][r] = -1;
						}
					}
					break;
				}
			}
		}
	}
}
