package irt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.uncommons.maths.random.GaussianGenerator;

import utility.MyUtil;

public class EstimationExceptTask {
	private ArrayList<double[]> AlphaR_Ests, TauR_Ests, Theta_Ests;

	private int maxLoop, burnIn, interbal, CurrentT;
	private IRTModel irt;
	private Random rand;
	private GaussianGenerator proposalDist;
	private double proposalSD = 0.05;

	public EstimationExceptTask(IRTModel irt, Random rand, HashMap<String, Integer> settings, int CurrentT) {
		this.irt = irt; this.rand = rand; this.CurrentT = CurrentT;
		this.maxLoop = settings.get("maxloop");
		this.burnIn = settings.get("burnin");
		this.interbal = settings.get("interbal");
		this.proposalDist =  new GaussianGenerator(0.0, proposalSD, rand);

		AlphaR_Ests = new ArrayList<double[]>();
		TauR_Ests = new ArrayList<double[]>();
		Theta_Ests = new ArrayList<double[]>();

		System.out.println("Estimating rater paramters and ability using up to data for task " + CurrentT);
		for(int i=0; i<this.maxLoop; i++){
			if(i % (maxLoop/10) == 0) System.out.println(" >> MCMC loop: " + i + " / " + maxLoop);
			updateThetaParam();
			updateAlphaR();
			updateTauR();
			if(i >= this.burnIn && (i%this.interbal) == 0){
				this.addMCMCSamples();
			}
		}
		calcPointEstimation();
		System.out.println("Estimation Finished");
		System.out.println("--------------------------");
	}

	void addMCMCSamples(){
		AlphaR_Ests.add(irt.alphaR.clone());
		TauR_Ests.add(irt.tauR.clone());
		Theta_Ests.add(irt.theta.clone());
	}

	void calcPointEstimation() {
		irt.alphaR = MyUtil.getAverages(AlphaR_Ests);
		irt.tauR = MyUtil.getAverages(TauR_Ests);
		irt.theta = MyUtil.getAverages(Theta_Ests);
	}

	private double posteriorTheta(int j) {
		return getLogLikelihoodTheta(j) + Math.log(MyUtil.getGausianValue(irt.theta_prior[0], irt.theta_prior[1], irt.theta[j]));
	}

	private double posteriorAlphaR(int r) {
		return getLogLikelihoodRater(r) + Math.log(MyUtil.getGausianValue(irt.alphaR_prior[0], irt.alphaR_prior[1], irt.alphaR[r]));
	}

	private double posteriorTauR(int r) {
		return getLogLikelihoodRater(r) + Math.log(MyUtil.getGausianValue(irt.tauR_prior[0], irt.tauR_prior[1], irt.tauR[r]));
	}

	private double getLogLikelihoodTheta(int j) {
		double LogLikelihood = 0.0;
		for (int t = 0; t < CurrentT; t++) {
			for (int r = 0; r < irt.J; r++) {
				LogLikelihood += Math.log(irt.getProb(j, t, r));
			}
		}
		return LogLikelihood;
	}

	private double getLogLikelihoodRater(int r) {
		double LogLikelihood = 0.0;
		for (int j = 0; j < irt.J; j++) {
			for (int t = 0; t < CurrentT; t++) {
				LogLikelihood += Math.log(irt.getProb(j, t, r));
			}
		}
		return LogLikelihood;
	}

	public void updateAlphaR() {
		for(int r=0; r<irt.R; r++){
			double prevAlphaR = irt.alphaR[r];
			double prevLikelihood = posteriorAlphaR(r);
			irt.alphaR[r] += proposalDist.nextValue();
			double newLikelihood = posteriorAlphaR(r);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.alphaR[r] = prevAlphaR;
			}
		}
	}

	public void updateTauR() {
		for(int r=0; r<irt.R; r++){
			double prevTauR = irt.tauR[r];
			double prevLikelihood = posteriorTauR(r);
			irt.tauR[r] += proposalDist.nextValue();
			double newLikelihood = posteriorTauR(r);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.tauR[r] = prevTauR;
			}
		}
	}

	public void updateThetaParam() {
		for (int j = 0; j < irt.J; j++) {
			double prevTheta = irt.theta[j];
			double prevLikelihood = posteriorTheta(j);
			irt.theta[j] += proposalDist.nextValue();
			double newLikelihood = posteriorTheta(j);
			double thresh = Math.exp(newLikelihood - prevLikelihood);
			if (MyUtil.isRejected(thresh, rand) == true) {
				irt.theta[j] = prevTheta;
			}
		}
	}
}
