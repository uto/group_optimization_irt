package irt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import utility.MyUtil;

public class CreateGroup {
	private int T, J, G, lower, upper, TiLim;
	private int[] grpMemberNum;
	private IRTModel irt;
	public int[][][] groups;

	public CreateGroup(int T, int J, int G, int TiLim, IRTModel irt) {
		this.T = T; this.J = J; this.G = G; this.TiLim = TiLim;
		this.irt = irt;
		lower = upper = J / G;
		if(J % G != 0) upper ++;
		grpMemberNum = new int[G];
		for (int g = 0; g < G; g++) grpMemberNum[g] = lower;
		for (int g = 0; g < J % G; g++) grpMemberNum[g]++;
		groups = new int[T][G][J];
	}

	public void setRandomGroup(int t) {
		System.out.println("Creating random groups for task " + t);
		ArrayList<Integer> candidate = new ArrayList<Integer>();
		for (int j = 0; j < J; j++) {
			candidate.add(j);
		}
		Collections.shuffle(candidate);
		int lastIdx = 0;
		for (int g = 0; g < G; g++) {
			for (int m = 0; m < grpMemberNum[g]; m++) {
				groups[t][g][candidate.get(lastIdx)] = 1;
				lastIdx++;
			}
		}
		System.out.println("Random group formation for task " + t + " is done.");
	}

	public void setOptimalGroupT(int t) {
		IloCplex cplex = null;
		double[][] FI = new double[J][J];
		for (int j = 0; j < J; j++) {
			for (int r = 0; r < J; r++) {
				FI[j][r] = irt.getInformation(t, irt.theta[j], r);
			}
		}
		System.out.println("Creating optimal groups for task " + t);
		try {
			cplex = new IloCplex();
			cplex.setParam(IloCplex.IntParam.NodeFileInd, 3);
			cplex.setParam(IloCplex.DoubleParam.TiLim, TiLim);
			cplex.setParam(IloCplex.Param.Simplex.Tolerances.Optimality, 1e-3);
			cplex.setParam(IloCplex.Param.Emphasis.MIP, IloCplex.MIPEmphasis.Optimality);
			cplex.setParam(IloCplex.Param.MIP.Tolerances.MIPGap, 1e-3);

			// initialize model's variable
			IloIntVar[][][] xVars = new IloIntVar[G][J][J];
	        for (int g = 0; g < G; g++) {
	            for (int j = 0; j < J; j++) {
	                for (int r = 0; r < J; r++) {
	                    xVars[g][j][r] = cplex.intVar(0, 1, String.format("x_%d_%d_%d", g, j, r));
	                }
	            }
	        }
	        int[][][] ratingTable = new int[G][J][J];

	        // constraint 1: one learner must belong to one group only
	        for (int j = 0; j < J; j++) {
	            IloLinearNumExpr learnerBnd = cplex.linearNumExpr();
	            for (int g = 0; g < G; g++) {
	                learnerBnd.addTerm(xVars[g][j][j], 1);
	            }
	            cplex.add(cplex.eq(learnerBnd, 1));
	        }
	        // constraint 2: A rater must rate for the other ones who belong to the same group only
	        for (int j = 0; j < J; j++) {
	            for (int r = 0; r < J; r++) {
	                if (j == r) continue;
	                IloLinearNumExpr rateBnd = cplex.linearNumExpr();
	                for (int g = 0; g < G; g++) {
	                    rateBnd.addTerm(xVars[g][j][r], 1);
	                }
	                cplex.add(cplex.le(rateBnd, 1));
	            }
	        }
	        // constraint 3: the number of learners in a group
	        for (int g = 0; g < G; g++) {
	            IloLinearNumExpr numLearnersBnd = cplex.linearNumExpr();
	            for (int j = 0; j < J; j++) {
	                numLearnersBnd.addTerm(xVars[g][j][j], 1);
	            }
	            cplex.add(cplex.and(cplex.ge(numLearnersBnd, lower), cplex.le(numLearnersBnd, upper)));
	        }
	        // constraint 4: the number of raters for a learner
	        for (int g = 0; g < G; g++) {
	            for (int j = 0; j < J; j++) {
	                IloNumExpr numRatesBnd = cplex.sum(xVars[g][j]);
	                IloConstraint x_gjj_1 = cplex.eq(xVars[g][j][j], 1);
	                IloConstraint x_gjj_0 = cplex.eq(xVars[g][j][j], 0);
	                cplex.add(cplex.ifThen(x_gjj_1, cplex.and(cplex.ge(numRatesBnd, lower), cplex.le(numRatesBnd, upper))));
	                cplex.add(cplex.ifThen(x_gjj_0, cplex.eq(numRatesBnd, 0)));
	            }
	        }
	        // constraint 5: how many times a learner can rate the other learners in the same group
	        for (int g = 0; g < G; g++) {
	            for (int r = 0; r < J; r++) {
	                IloLinearNumExpr rateTimesBnd = cplex.linearNumExpr();
	                for (int j = 0; j < J; j++) {
	                    rateTimesBnd.addTerm(xVars[g][j][r], 1);
	                }
	                IloConstraint x_grr_1 = cplex.eq(xVars[g][r][r], 1);
	                IloConstraint x_grr_0 = cplex.eq(xVars[g][r][r], 0);
	                cplex.add(
                        cplex.ifThen(x_grr_1, cplex.and(cplex.ge(rateTimesBnd, lower), cplex.le(rateTimesBnd, upper)))
	                );
	                cplex.add(cplex.ifThen(x_grr_0, cplex.eq(rateTimesBnd, 0)));
	            }
	        }
	        // constraint 6: two learners in a group must rate each other
	        for (int g = 0; g < G; g++) {
	            for (int j = 0; j < J; j++) {
	                for (int r = 0; r < J; r++) {
	                    if (j == r) continue;
	                    cplex.add(cplex.eq(xVars[g][j][r], xVars[g][r][j]));
	                }
	            }
	        }

	        // build objective function
			IloNumVar[] y = new IloIntVar[J];
			for (int j = 0; j < J; j++) {
				IloNumExpr[] FI_j = new IloNumExpr[G];
				for (int g = 0; g < G; g++) {
					IloLinearNumExpr FI_jg = cplex.linearNumExpr();
					for (int r = 0; r < J; r++) {
						if (j == r) continue;
						FI_jg.addTerm(FI[j][r], xVars[g][j][r]);
					}
					FI_j[g] = FI_jg;
				}
				y[j] = cplex.numVar(0.0, Double.MAX_VALUE, String.format("y_%d", j));
				cplex.addGe(cplex.sum(FI_j), y[j]);
			}
			cplex.addMaximize(cplex.sum(y));

			boolean isSolved = cplex.solve();
			if (isSolved) {
		        groups[t] = new int[G][J];
		        for (int g = 0; g < xVars.length; g++) {
		            for (int j = 0; j < xVars[g].length; j++) {
		                for (int r = 0; r < J; r++) {
		                    double x_gjr = cplex.getValue(xVars[g][j][r]);
		                    ratingTable[g][j][r] = (Math.abs(x_gjr - 1.0) <= 1e-3) ? 1 : 0;
		                }
		                groups[t][g][j] = ratingTable[g][j][j] == 1 ? 1 : 0;
		            }
		        }
			} else {
				throw new Exception("Could not optimize next groups");
			}
		} catch (IloException e) {
			try {
				throw new Exception(String.format("Concert exception %s caught", e.getMessage()));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cplex.end();
			cplex = null;
		}
		System.out.println("Optimal group formation for task " + t + " is done");
	}

	public void printGroups(String filename){
		try {
			PrintWriter bw = MyUtil.Writer(filename);
			for (int t = 0; t < T; t++) {
				bw.println("task" + (t+1));
				for (int g = 0; g < G; g++) {
					String line = "";
					for(int j=0;j<J;j++){
						if(groups[t][g][j] == 1) line += (j+1) +",";
					}
					bw.println(" Group(" + (g+1) + ") = {" + line.substring(0, line.length()-1) +"}");
				}
				bw.println("");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
