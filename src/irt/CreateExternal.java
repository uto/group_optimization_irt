package irt;

import java.io.IOException;
import java.io.PrintWriter;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import utility.MyUtil;

public class CreateExternal {
	private int T, J, nJ, ne, TiLim;
	private IRTModel irt;
	public int[][][] selectedRaters;

	public CreateExternal(int T, int J, int nJ, int ne, int TiLim, IRTModel irt) {
		this.T = T; this.J = J; this.TiLim = TiLim;
		this.nJ = nJ; this.ne = ne;
		this.irt = irt;
        selectedRaters = new int[T][J][J];
	}

	public void setExternalT(int t, int[][] groups) {
		System.out.println("Selecting external raters for task " + t);
		IloCplex cplex = null;
		double[][] FI = new double[J][J];
		for (int j = 0; j < J; j++) {
			for (int r = 0; r < J; r++) {
				FI[j][r] = irt.getInformation(t, irt.theta[j], r);
			}
		}
		try {
			cplex = new IloCplex();
			cplex.setParam(IloCplex.IntParam.NodeFileInd, 3);
			cplex.setParam(IloCplex.DoubleParam.TiLim, TiLim);
			cplex.setParam(IloCplex.Param.Simplex.Tolerances.Optimality, 1e-3);
			cplex.setParam(IloCplex.Param.Emphasis.MIP, IloCplex.MIPEmphasis.Optimality);
			cplex.setParam(IloCplex.Param.MIP.Tolerances.MIPGap, 1e-3);

			// initialize model's variable
			IloIntVar[][] x = new IloIntVar[J][J];
			for (int j = 0; j < J; j++) {
				for (int r = 0; r < J; r++) {
					x[j][r] = cplex.boolVar(String.format("x_%d_%d", j, r));
	                if (j == r) {
	                    cplex.addEq(x[j][r], 0);
	                }
				}
			}

			// Constraint 1
	        IloLinearNumExpr[] numRatersBound = new IloLinearNumExpr[J];
	        for (int j = 0; j < J; j++) {
	            numRatersBound[j] = cplex.linearNumExpr();
	            for (int r = 0; r < J; r++) {
	                numRatersBound[j].addTerm(x[j][r], 1);
	            }
	            cplex.add(cplex.eq(numRatersBound[j], ne));
	        }

			// Constraint 2
	        for (int j = 0; j < J; j++) {
	            for (int[] interRaters : groups) {
	                if (interRaters[j] == 1) {
	                    for (int r = 0; r < interRaters.length; r++) {
	                        if (interRaters[r] == 1) cplex.addEq(x[j][r], 0);
	                    }
	                    break;
	                }
	            }
	        }

			// Constraint 3
	        IloLinearNumExpr[] numRatingBound = new IloLinearNumExpr[J];
	        for (int r = 0; r < J; r++) {
	            numRatingBound[r] = cplex.linearNumExpr();
	            for (int j = 0; j < J; j++) {
	                numRatingBound[r].addTerm(x[j][r], 1);
	            }
	            cplex.add(cplex.le(numRatingBound[r], nJ));
	        }

			// Objective function
	        IloLinearNumExpr[] FI_j = new IloLinearNumExpr[J];
	        IloNumVar[] y = new IloIntVar[J];
	        for (int j = 0; j < J; j++) {
	            y[j] = cplex.numVar(0.0, Double.MAX_VALUE, String.format("y_%d", j));
	            FI_j[j] = cplex.linearNumExpr();
	            for (int r = 0; r < J; r++) {
	                if (j == r) continue;
	                FI_j[j].addTerm(FI[j][r], x[j][r]);
	            }
	            cplex.add(cplex.ge(FI_j[j], y[j]));
	        }
	        cplex.addMaximize(cplex.sum(y));

			boolean isSolved = cplex.solve();
			if (isSolved) {
	            for (int j = 0; j < J; j++) {
	                for (int r = 0; r < J; r++) {
	                    double x_jr = cplex.getValue(x[j][r]);
	                    selectedRaters[t][j][r] = (Math.abs(x_jr - 1.0) <= 1e-3) ? 1 : 0;
	                }
	            }
			} else {
				throw new Exception("Could not optimize next groups");
			}
		} catch (IloException e) {
			try {
				throw new Exception(String.format("Concert exception %s caught", e.getMessage()));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cplex.end();
			cplex = null;
		}
		System.out.println("External rater selection for task " + t +" is done.");
	}

	public void printSelection(String filename) {
		try {
			PrintWriter bw = MyUtil.Writer(filename);
			for (int t = 1; t < T; t++) {
				bw.println("task" + (t+1));
				for (int j = 0; j < J; j++) {
					String line = "";
					for(int r=0;r<J;r++){
						if(selectedRaters[t][j][r] == 1) line += (r+1) +",";
					}
					bw.println(" learner(" + (j+1) + ") = {" + line.substring(0, line.length()-1)+"}");
				}
				bw.println("");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
