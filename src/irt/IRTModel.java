package irt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.uncommons.maths.random.GaussianGenerator;

import utility.MyUtil;

public class IRTModel {
	public int J, T, R, K;

	public double[] alphaT, alphaR, tauR, theta;
	public double[][] betaTK;

	public final double[] alphaT_prior = { 0.0, 0.4 };
	public final double[] alphaR_prior = { 0.0, 0.4 };
	public final double[] tauR_prior = { 0.0, 1.0 };
	public final double[] theta_prior = { 0.0, 1.0 };
	public final double[] betaTK_priorMean = { -2.0, -0.75, 0.75, 2.0 };
	public final double[][] betaTK_priorSD = { { 0.16, 0.10, 0.04, 0.04 }, { 0.10, 0.16, 0.10, 0.04 },
			{ 0.04, 0.10, 0.16, 0.10 }, { 0.04, 0.04, 0.10, 0.16 } };
	public Random rand;
	private Data d;

	public IRTModel(int J, int T, int R, int K, Random rand) {
		this.J = J; this.T = T; this.R = R; this.K = K;
		this.rand = rand;
		alphaT = new double[T];
		betaTK = new double[T][K];
		alphaR = new double[R];
		tauR = new double[R];
		theta = new double[J];
	}

	public void setRandomParameters() {
		setInitParam(theta, theta_prior);
		setInitParam(alphaT, alphaT_prior);
		setInitParam(alphaR, alphaR_prior);
		setInitParam(tauR, tauR_prior);
		setBetaIK();
		alphaR = MyUtil.normalize(alphaR);
		tauR = MyUtil.normalize(tauR);
	}

	public void setInitParam(double[] param, double[] prior) {
		GaussianGenerator gen = new GaussianGenerator(prior[0], prior[1], rand);
		for (int i = 0; i < param.length; i++) {
			param[i] = gen.nextValue();
		}
	}

	public void setBetaIK() {
		for (int t = 0; t < T; t++) {
			MultivariateNormalDistribution gaussian = new MultivariateNormalDistribution(betaTK_priorMean, betaTK_priorSD);
			double[] bHyp = gaussian.sample();
			Arrays.sort(bHyp);
			for (int k = 0; k < bHyp.length; k++) {
				betaTK[t][k] = bHyp[k];
			}
			betaTK[t][K - 1] = 0.0;
		}
	}

	double[] IRC(int t, double theta, int r) {
		double[] BCC = new double[K - 1];
		double[] IRC = new double[K];
		for (int k = 0; k < K; k++) {
			if (k == 0) {
				BCC[k] = BCC(t, theta, r, k);
				IRC[k] = 1.0 - BCC[k];
			} else if (k == (K - 1)) {
				IRC[k] = BCC[k - 1];
			} else {
				BCC[k] = BCC(t, theta, r, k);
				IRC[k] += BCC[k - 1] - BCC[k];
			}
		}
		return IRC;
	}

	double BCC(int t, double theta, int r, int k) {
		double logit = Math.exp(alphaT[t]) * Math.exp(alphaR[r]) * (theta - betaTK[t][k] - tauR[r]);
		return 1.0 / (1.0 + Math.exp(-logit));
	}

	double getProb(int j, int t, int r) {
		int k = this.d.U[j][t][r];
		if (k != -1) {
			return IRC(t, theta[j], r)[k];
		}
		return 1.0;
	}

	public void setData(String file) {
       	this.d = new Data(this, rand);
       	d.readData(file);
	}

	public Data getData() {
		return this.d;
	}

	public double getInformation(int t, double theta, int r) {
		double[] BCC = new double[K + 1];
		for (int k = 0; k < K + 1; k++) {
			if (k == 0) BCC[k] = 1;
			else if (k == K) BCC[k] = 0;
			else BCC[k] = BCC(t, theta, r, k - 1);
		}
		double inf = 0;
		for (int k = 0; k < K; k++) {
			double f0 = BCC[k] * (1 - BCC[k]) - BCC[k + 1] * (1 - BCC[k + 1]);
			double diff = (BCC[k] - BCC[k + 1]);
			if(diff < 0.0000001) diff = 0.0000001;
			inf += Math.pow(f0, 2) / diff;
		}
		inf *= Math.pow(Math.exp(alphaT[t]), 2) * Math.pow(Math.exp(alphaR[r]), 2);
		return inf;
	}

	// IO
	public void printParameters(String filename) {
		try {
			int digit = 5;
			PrintWriter bw = MyUtil.Writer(filename);
			bw.println("log alphaT," + MyUtil.doublesTostring(alphaT, digit));
			for (int t = 0; t < betaTK.length; t++) {
				bw.println("beta" + t + "K," + MyUtil.doublesTostring(betaTK[t], digit));
			}
			bw.println("log gammaR," + MyUtil.doublesTostring(alphaR, digit));
			bw.println("epsilonR," + MyUtil.doublesTostring(tauR, digit));
			bw.println("theta," + MyUtil.doublesTostring(theta, digit));
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void readTaskParameters(String filename) {
		try {
			BufferedReader br = MyUtil.Reader(filename);
			alphaT = readParamString(br.readLine().split(","));
			for (int t = 0; t < betaTK.length; t++) {
				betaTK[t] = readParamString(br.readLine().split(","));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private double[] readParamString(String[] line){
		double[] d = new double[line.length-1];
		for(int i=0;i<d.length;i++){
			d[i] = Double.valueOf(line[i+1]);
		}
		return d;
	}
}
