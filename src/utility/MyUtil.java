package utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;

public class MyUtil {

	public static double getGausianValue(double m, double s, double v) {
		double ee = -Math.pow(v - m, 2) / (2 * s * s);
		return Math.exp(ee) / (Math.sqrt(2 * Math.PI) * s);
	}

	public static double getMultGausianValue(double[] m, double[][] s, double[] v) {
		MultivariateNormalDistribution mnd = new MultivariateNormalDistribution(m, s);
		return mnd.density(v);
	}

	public static double[] normalize(double[] d) {
		double[] normalized = new double[d.length];
		double sum = 0;
		for (int i = 0; i < d.length; i++) {
			sum += d[i];
		}
		sum = sum / d.length;
		for (int i = 0; i < d.length; i++) {
			normalized[i] = d[i] - sum;
		}
		return normalized;
	}

	public static double[] removeDimension4GRM(double[] v) {
		double[] d = new double[v.length - 1];
		for (int i = 0; i < d.length; i++) {
			d[i] = v[i];
		}
		return d;
	}

	public static boolean isRejected(double thresh, Random rand) {
		boolean isRejected = false;
		if (thresh < 1.0) {
			double t = rand.nextDouble();
			if (t > thresh) {
				isRejected = true;
			}
		}
		return isRejected;
	}

	public static double[] parseLogToExp(double[] param) {
		double[] parsed = new double[param.length];
		for (int i = 0; i < param.length; i++) {
			parsed[i] = Math.exp(param[i]);
		}
		return parsed;
	}

	public static PrintWriter Writer(String fileName) throws IOException {
		return new PrintWriter(new BufferedWriter(new FileWriter(new File(fileName))));
	}

	public static BufferedReader Reader(String fileName) throws FileNotFoundException {
		return new BufferedReader(new FileReader(new File(fileName)));
	}

	public static void print(String str) {
		System.out.println(str);
	}

	public static String trimArray(int[] ar) {
		return Arrays.toString(ar).replace("[", "").replace("]", "").replace(" ", "");
	}

	public static String trimArray(double[] ar) {
		return Arrays.toString(ar).replace("[", "").replace("]", "").replace(" ", "");
	}

	public static String trimArray(String[] ar) {
		return Arrays.toString(ar).replace("[", "").replace("]", "").replace(" ", "");
	}

	public static String trimArray(String ar) {
		return ar.replace("[", "").replace("]", "").replace(" ", "");
	}

	public static String doublesTostring(double[] t, int digit) {
		String line = "";
		for (int i = 0; i < t.length; i++) {
			line += t[i] + ",";
		}
		return line.substring(0, line.length() - 1);
	}

	public static double[] getAverages(ArrayList<double[]> array) {
		int l = array.get(0).length;
		double[] ave = new double[l];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < l; j++) {
				ave[j] += array.get(i)[j];
			}
		}
		for (int j = 0; j < l; j++) {
			ave[j] /= array.size();
		}
		return ave;
	}

	public static double[][] getAveMatrix(ArrayList<double[][]> array) {
		int l = array.get(0).length;
		int c = array.get(0)[0].length;
		double[][] ave = new double[l][c];
		for (int i = 0; i < array.size(); i++) {
			for (int j = 0; j < l; j++) {
				for (int k = 0; k < c; k++) {
					ave[j][k] += array.get(i)[j][k];
				}
			}
		}
		for (int j = 0; j < l; j++) {
			for (int k = 0; k < c; k++) {
				ave[j][k] /= array.size();
			}
		}
		return ave;
	}
}
