package controller;

import java.util.HashMap;
import java.util.Random;

import org.uncommons.maths.random.MersenneTwisterRNG;

import irt.CreateExternal;
import irt.CreateGroup;
import irt.Estimation;
import irt.EstimationExceptTask;
import irt.IRTModel;

public class MainCtl {
	// Parameters about peer assessment data
	final int learners = 34;
	final String peerAssessData = "data/input/peerAssessData.csv";

	// Parameters about data for task parameter estimation
	final int learners_in_taskEstData = 34, raters_in_taskEstData = 5;
	final String taskEstDataFile = "data/input/fiveRatersData.csv";
	final String taskParamsFile = "data/input/taskParameters.csv";

	// Common parameters
	final int tasks = 4, categories = 5, groups=3, ne = 3, nJ = 12, TiLim = 600;
	final String outputDir = "data/output/";
	HashMap<String, Integer>  mcmcSettings;
	Random rand;

	public MainCtl() {
		mcmcSettings = new HashMap<String, Integer>();
		mcmcSettings.put("maxloop", 50000);
		mcmcSettings.put("burnin",  30000);
		mcmcSettings.put("interbal",  100);
		rand = new MersenneTwisterRNG();

		/* -----------------------------------------
		 *  runTaskParameterEstimation() estimates the task parameters
		 *  from taskEstDataFile. If the parameters have been already
		 *  estimated, the following code is not required.
		 ----------------------------------------- */
		// runTaskParameterEstimation();

		/* -----------------------------------------
		 *  createGroupsAndExtenalRaters() estimates the rater parameters
		 *  and ability of learners from peerAssessData based on the groups
		 *  and external raters which were created by the proposed method.
		 ----------------------------------------- */
		createGroupsAndExtenalRaters();
	}

	void runTaskParameterEstimation(){
		IRTModel irt = new IRTModel(learners_in_taskEstData, tasks, raters_in_taskEstData, categories, rand);
		irt.setData(taskEstDataFile);
		new Estimation(irt, rand, mcmcSettings);
		irt.printParameters(taskParamsFile);
	}

	void createGroupsAndExtenalRaters(){
		IRTModel irt = new IRTModel(learners, tasks, learners, categories, rand);
		irt.readTaskParameters(taskParamsFile);
		irt.setData(peerAssessData);
		CreateGroup grouping = new CreateGroup(tasks, learners, groups, TiLim, irt);
		CreateExternal external = new CreateExternal(tasks, learners, nJ, ne, TiLim, irt);
		for(int t=0;t<tasks;t++){
			if(t == 0){
				grouping.setRandomGroup(t);
				irt.getData().setGrouping(grouping.groups, groups, t);
				new EstimationExceptTask(irt, rand, mcmcSettings, t+1);
			} else {
				grouping.setOptimalGroupT(t);
				external.setExternalT(t, grouping.groups[t]);
				irt.getData().setGroupingWithExternal(grouping.groups, external.selectedRaters, groups, t);
				new EstimationExceptTask(irt, rand, mcmcSettings, t+1);
			}
			irt.printParameters(outputDir + "parameter_task"+(t+1)+".csv");
		}
		grouping.printGroups(outputDir + "groups.csv");
		external.printSelection(outputDir + "external.csv");
	}

	public static void main(String[] args) {
		new MainCtl();
	}
}
