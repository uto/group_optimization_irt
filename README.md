This package includes programs for the parameter estimation of the IRT model with rater parameters, the group formation optimization, and the external rater assignment method for peer asseessment. The codes are written in Java, and requires the IBM ILOG Cplex.

The "/controller/MainCtl.java" file is the main file. 
The program requires two datasets, one is for estimating the task parameters, and the other is for testing the proposed methods.
In this repository, the datasets used in the paper have been set. 
Each data should be stored in "data/input/" directory. The columns of the data indicate as follows.

 Column 1: Index of tasks (start from 0)
 Column 2: Index of learners (start from 0)
 Column 3: Index of raters (start from 0)
 Column 4: Rating score ({0,1,2,3,4}; -1 indicates missing data)

The results will be written in "/output/" directory as the following three types of files.

parameter_task[t].csv: it shows the IRT parameter values estimated using the data up to t tasks. 
groups.csv: it presents the groups created for each task.
external.csv: it gives the external raters assgined to each learners in each task.